package ru.nlmk.study.jse74.task1.io;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.mock.env.MockEnvironment;
import ru.nlmk.study.jse74.task1.ApplicationConfig;

import java.lang.annotation.Annotation;

import static org.junit.jupiter.api.Assertions.*;

class DataHandlerTest {
    @Test
    public void someTest(){
/*        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        MockEnvironment mockEnvironment = new MockEnvironment().withProperty("someGroup.someProperty", "someValue");
        context.setEnvironment(mockEnvironment);
        context.registerBean(ApplicationConfig.class);
        context.refresh();*/

        MockEnvironment mockEnvironment = new MockEnvironment().withProperty("someGroup.someProperty", "someValue");
        Downloader downloader = Mockito.mock(Downloader.class);
        Uploader uploader = Mockito.mock(Uploader.class);
        DataHandler dataHandler = new DataHandler(downloader, uploader, mockEnvironment);

        assertEquals("someValue", dataHandler.getSomeData());
    }
}