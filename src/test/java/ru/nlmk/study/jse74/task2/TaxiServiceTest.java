package ru.nlmk.study.jse74.task2;

import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.ReflectionUtils;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TaxiServiceTest {
    @Test
    void testInjecting (){
        Car car = new Car();
        ReflectionTestUtils.setField(car, "id", 1);
        car.setModel("Tesla model X");

        RepairService repairService = mock(RepairService.class);
        when(repairService.getCarStatus(any())).thenReturn("in repairment");
        TaxiService taxiService = new TaxiService();

        ReflectionTestUtils.setField(taxiService, "repairService", repairService);

        assertTrue("Car 1 status: in repairment".equals(taxiService.callCarForRide(1)));
    }
}