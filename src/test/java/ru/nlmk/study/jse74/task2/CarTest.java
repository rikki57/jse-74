package ru.nlmk.study.jse74.task2;

import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

class CarTest {
    @Test
    void testNonPublidField(){
        Car car = new Car();
        ReflectionTestUtils.setField(car, "id", 10);

        assertEquals(10, car.getId());
    }

    @Test
    void testMethodInvoke(){
        Car car = new Car();
        ReflectionTestUtils.setField(car, "id", 1);
        car.setModel("Tesla model X");

        assertTrue("id: 1; model: Tesla model X".equals(ReflectionTestUtils.invokeMethod(car, "carToString")));
    }
}