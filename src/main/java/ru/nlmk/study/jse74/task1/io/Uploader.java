package ru.nlmk.study.jse74.task1.io;

public interface Uploader {
    boolean upload(String path, Object content);
}
