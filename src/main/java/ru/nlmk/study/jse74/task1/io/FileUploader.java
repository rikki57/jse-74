package ru.nlmk.study.jse74.task1.io;

public class FileUploader implements Uploader{
    @Override
    public boolean upload(String path, Object content) {
        System.out.println(new StringBuilder("Content ")
                .append(content.toString())
                .append(" was uploaded to path ")
                .append(path));
        return true;
    }
}
