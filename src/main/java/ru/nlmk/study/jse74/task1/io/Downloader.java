package ru.nlmk.study.jse74.task1.io;

public interface Downloader {
    String download(String path);
}
