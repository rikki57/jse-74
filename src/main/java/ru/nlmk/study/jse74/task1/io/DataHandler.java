package ru.nlmk.study.jse74.task1.io;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class DataHandler {
    private Downloader downloader;
    private Uploader uploader;
    private Environment environment;

    @Autowired
    public DataHandler(Downloader downloader, Uploader uploader, Environment environment) {
        this.downloader = downloader;
        this.uploader = uploader;
        this.environment = environment;
    }

    public void handleData(String srcPath, String destPath){
        String content = this.downloader.download(srcPath);
        String handledContent = content.toLowerCase();
        this.uploader.upload(destPath, handledContent);
    }

    public String getSomeData(){
        return environment.getProperty("someGroup.someProperty");
    }
}
