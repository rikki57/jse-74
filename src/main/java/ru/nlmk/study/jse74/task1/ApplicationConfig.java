package ru.nlmk.study.jse74.task1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.nlmk.study.jse74.task1.io.*;

@Configuration
@ComponentScan("ru.nlmk.study.jse74.task1")
public class ApplicationConfig {
    @Bean
    @Profile("development")
    public Downloader getDevelopmentDownloader(){
        return new DBDownloader();
    }

    @Bean
    @Profile("production")
    public Downloader getProductionDownloader(){
        return new FileDownloader();
    }

    @Bean
    public Uploader getUploader(){
        return new FileUploader();
    }
}
