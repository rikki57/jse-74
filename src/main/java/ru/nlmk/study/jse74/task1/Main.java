package ru.nlmk.study.jse74.task1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.nlmk.study.jse74.task1.io.DataHandler;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ru.nlmk.study.jse74.task1.ApplicationConfig.class);
        DataHandler dataHandler = (DataHandler) applicationContext.getBean("dataHandler");
        dataHandler.handleData("somePath", "someDest");
    }
}
