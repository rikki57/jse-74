package ru.nlmk.study.jse74.task3.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.awt.*;

@Controller
public class MyController {
    @GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    public String index(Model model){
        model.addAttribute("data", "Some data");
        return "index";
    }

    @GetMapping(value = "/anotherpage", produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public String anotherPage(){
        return "Another page";
    }
}
