package ru.nlmk.study.jse74.task2;

import org.springframework.beans.factory.annotation.Autowired;

public class TaxiService {
    @Autowired
    RepairService repairService;

    public String callCarForRide(Integer carId){
        return "Car " + carId + " status: " + repairService.getCarStatus(carId);
    }
}
