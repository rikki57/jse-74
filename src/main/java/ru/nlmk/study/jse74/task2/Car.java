package ru.nlmk.study.jse74.task2;

public class Car {
    private String model;
    private Integer id;

    public String getModel() {
        return model;
    }

    public Integer getId() {
        return id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    private String carToString(){
        return "id: " + getId() + "; model: " + getModel();
    }
}
